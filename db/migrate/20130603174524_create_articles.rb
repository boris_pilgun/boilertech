class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title_ua, :limit => 255
      t.string :title_ru, :limit => 255
      t.text :content_ua
      t.text :content_ru
      t.string :keyword, :limit => 255
      t.boolean :just_block, :default => false
      t.boolean :active, :default => false


      t.timestamps
    end

    add_index :articles, :keyword, :unique => true
  end
end
