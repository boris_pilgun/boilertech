class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.text :description
      t.string :permalink, :limit => 255, :null => false

      t.timestamps
    end

    add_index :pages, :permalink, :unique => true
  end
end
