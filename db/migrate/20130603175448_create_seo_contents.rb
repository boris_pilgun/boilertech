class CreateSeoContents < ActiveRecord::Migration
  def change
    create_table :seo_contents do |t|
      t.string :seo_title_ua, :limit => 255
      t.string :seo_title_ru, :limit => 255
      t.string :seo_keywords_ua, :limit => 255
      t.string :seo_keywords_ru, :limit => 255
      t.string :seo_description_ua, :limit => 255
      t.string :seo_description_ru, :limit => 255
      t.integer :optimized_id, :null => false
      t.string :optimized_type, :null => false

      t.timestamps
    end

    add_index :seo_contents, [:optimized_id, :optimized_type], :unique => true, :name => 'unique_seo_object'
  end
end
