class CreateArticlesPages < ActiveRecord::Migration
  def change
    create_table :articles_pages, :id => false do |t|
      t.integer :article_id, :null => false
      t.integer :page_id, :null => false
      t.integer :position

      t.timestamps
    end

    add_index :articles_pages, [:article_id, :page_id], :unique => true, :name => 'article_page_pair'
  end
end
