# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :seo_content do
    seo_title_ua "MyString"
    seo_title_ru "MyString"
    seo_keywords_ua "MyString"
    seo_keywords_ru "MyString"
    seo_description_ua "MyString"
    seo_description_ru "MyString"
    optimized_id 1
    optimized_type "MyString"
  end
end
