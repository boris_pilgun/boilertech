# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :article do
    title_ua "MyString"
    title_ru "MyString"
    content_ua "MyText"
    content_ru "MyText"
    keyword "MyString"
    just_block false
  end
end
