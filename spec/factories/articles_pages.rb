# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :articles_page, :class => 'ArticlesPages' do
    article_id 1
    page_id ""
    position 1
  end
end
