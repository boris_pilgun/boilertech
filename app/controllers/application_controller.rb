class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :set_locale
  before_filter :default_redirect

  #layout 'readable'

  def set_locale
    if request.path.start_with?("/boileradmin")
      I18n.locale = 'en'
    elsif ['ru', 'ua'].include?(extract_locale_from_accept_language_header)
      I18n.locale = extract_locale_from_accept_language_header
    else I18n.locale = "ua"
    end

  end

  def extract_locale_from_accept_language_header
    request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
  end

  def default_redirect
    if request.get? && !params[:locale]
      params[:locale] = I18n.locale.to_s
      return redirect_to params, :status => 301
    end
  end

end
