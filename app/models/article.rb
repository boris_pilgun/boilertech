class Article < ActiveRecord::Base
  attr_accessible :content_ru, :content_ua, :just_block, :keyword, :title_ru, :title_ua, :active

  has_and_belongs_to_many :pages, :through => :articles_pages, :foreign_key => :article_id
  has_many :articles_pages, :foreign_key => :article_id
  has_one :seo_content, :dependent => :destroy


  validates :just_block, :keyword, :active, :presence => true
  validates :content_ru, :length => {:minimum => 2}, :allow_nil => true
  validates :content_ua, :length => {:minimum => 2}, :allow_nil => true
  validates :title_ua, :length => {:minimum => 2, :maximum => 255}, :allow_nil => true
  validates :title_ru, :length => {:minimum => 2, :maximum => 255}, :allow_nil => true
  validates :keyword, :uniqueness => true, :length => {:minimum => 3, :maximum => 255}

  after_create :add_seo_content
  before_destroy { pages.clear }  #clear join table

  def add_seo_content
    self.create_seo_content
  end

  def title(lang)
    return '' unless ['ua', 'ru'].includes? lang.to_s.downcase
    self.send("title_#{lang.to_s.downcase}")
  end

  def content(lang)
    return '' unless ['ua', 'ru'].includes? lang.to_s.downcase
    self.send("content_#{lang.to_s.downcase}")
  end
end
