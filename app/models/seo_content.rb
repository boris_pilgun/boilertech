class SeoContent < ActiveRecord::Base
  attr_accessible :optimized_id, :optimized_type, :seo_description_ru, :seo_description_ua, :seo_keywords_ru, :seo_keywords_ua, :seo_title_ru, :seo_title_ua

  belongs_to :optimized, :polymorphic => true

  validates :optimized_id, :optimized_type, :presence => true
  validates :optimized_id, :uniqueness => {:scope => :optimized_type}
  validates :seo_description_ua, :length => {:minimum => 2}, :allow_nil => true
  validates :seo_description_ru, :length => {:minimum => 2}, :allow_nil => true
  validates :seo_keywords_ua, :length => {:minimum => 2}, :allow_nil => true
  validates :seo_keywords_ru, :length => {:minimum => 2}, :allow_nil => true
  validates :seo_title_ua, :length => {:minimum => 2}, :allow_nil => true
  validates :seo_title_ru, :length => {:minimum => 2}, :allow_nil => true
end
