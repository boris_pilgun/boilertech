class ArticlesPages < ActiveRecord::Base
  attr_accessible :article_id, :page_id, :position

  belongs_to :articles
  belongs_to :pages

  validates :article_id, :page_id, :presence => true
  validates :article_id, :uniqueness => {:scope => :page_id}
  validates :position, :uniqueness => {:scope => :page_id}
end

