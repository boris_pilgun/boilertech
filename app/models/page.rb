class Page < ActiveRecord::Base
  attr_accessible :description, :permalink

  has_and_belongs_to_many :articles, :through => :articles_pages, :foreign_key => :page_id
  has_many :articles_pages, :foreign_key => :page_id
  has_one :seo_content, :dependent => :destroy

  validates :permalink, :presence => true, :uniqueness => true, :length => {:minimum => 3, :maximum => 100}

  after_create :add_seo_content
  before_destroy { articles.clear }  #clear join table

  def add_seo_content
    self.create_seo_content
  end
end
